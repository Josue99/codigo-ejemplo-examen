package com.parraga.josue.practicaexamen.Adaptador;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parraga.josue.practicaexamen.Modelo.Productos;
import com.parraga.josue.practicaexamen.R;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class AdaptadorProductos extends RecyclerView.Adapter<AdaptadorProductos.MyViewHolder>{

    private ArrayList<Productos> productos;

    public AdaptadorProductos(ArrayList<Productos> productos){
        this.productos = productos;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_productos, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {

        Productos productos1 = productos.get(position);
        myViewHolder.name.setText(productos1.getName());
        myViewHolder.id.setText(productos1.getId());
        Picasso.get().load(productos1.getFoto()).into(myViewHolder.foto);
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView foto;
        private TextView name, id;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            foto = (ImageView)itemView.findViewById(R.id.Foto);
            name = (TextView)itemView.findViewById(R.id.Nombre);
            id = (TextView)itemView.findViewById(R.id.Id);
        }
    }
}
