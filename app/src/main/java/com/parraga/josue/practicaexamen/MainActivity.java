package com.parraga.josue.practicaexamen;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.parraga.josue.practicaexamen.Adaptador.AdaptadorProductos;
import com.parraga.josue.practicaexamen.Modelo.Productos;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private static final String URL_PRODUCTOS = "http://10.22.8.172:3000/productos/";
    private RecyclerView.LayoutManager layoutManager;
    private AdaptadorProductos adaptadorProductos;
    private ArrayList<Productos> arrayListP;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.Productos);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        arrayListP = new ArrayList<>();
        adaptadorProductos = new AdaptadorProductos(arrayListP);
        progressDialog = new ProgressDialog(this);

        ObtenerProductos();
    }
    private void ObtenerProductos(){
        progressDialog.setCancelable(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_PRODUCTOS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response", response);
                progressDialog.dismiss();
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Productos productos = new Productos();
                        productos.setFoto(jsonObject.getString("foto"));
                        productos.setId(jsonObject.getString("id"));
                        productos.setName(jsonObject.getString("name"));
                        arrayListP.add(productos);
                    }
                    recyclerView.setAdapter(adaptadorProductos);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Ocurrio un error mientras se cargaba", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
